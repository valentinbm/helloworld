﻿using Grpc.Core;
using Grpc.Net.Client;
using HelloWorld;
using System;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            string name = "";

            Console.WriteLine("Please, enter your name: ");

            name = Console.ReadLine();

            var input = new HelloRequest { Name = name };

            var channel = GrpcChannel.ForAddress("https://localhost:5001");

            var client = new Greeter.GreeterClient(channel);

            var reply = await client.SayHelloAsync(input);

        }
    }
}
